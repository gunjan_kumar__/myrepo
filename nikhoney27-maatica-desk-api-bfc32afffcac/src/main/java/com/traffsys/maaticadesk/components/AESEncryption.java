package com.traffsys.maaticadesk.components;

/**
 * @author Nikhil Sharma
 * @Date : 25-Nov-2017
 * @Description :
 */
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.traffsys.maaticadesk.utils.AppConstant;

public class AESEncryption {

	public static final String ERROR_WHILE_ENCRYPTING = "Error while encrypting:";
	public static final String ERROR_WHILE_DECRYPTING = "Error while decrypting:";
	public static final String AES_ECB_PKCS_PADDING = "AES/ECB/PKCS5PADDING";
	// public static final String AES_ECB_PKCS5_PADDING1 =
	// "AESEncryption/ECB/PKCS5Padding";
	public static final String UTF_ENCODING = "UTF-8";
	public static final String SHA_ONE = "SHA-1";
	public static final int NEW_LENGTH = 16;
	public static final String AESPARAM = "AES";
	private static SecretKeySpec sEcretKey;
	private static final String salt = "@#$%^&*()";
	// private static final String mKey =
	// "B374A26A71490437AA024E4FADD5B497FDFF1A8EA6FF12F6FB65AF2720B59CCF";
	private static byte[] sKeys;

	public static void setKey(final String key) {
		final String myKey = salt + key;
		MessageDigest sha = null;
		try {
			try {
				sKeys = myKey.getBytes(UTF_ENCODING);
			} catch (final UnsupportedEncodingException e) {
				Logger.getPathInstance().log(e.getMessage(), AESEncryption.class.getSimpleName(), AppConstant.ERROR,
						e.getStackTrace()[0].getLineNumber());
				e.printStackTrace();
			}
			sha = MessageDigest.getInstance(SHA_ONE);
			sKeys = sha.digest(sKeys);
			sKeys = Arrays.copyOf(sKeys, NEW_LENGTH);
			sEcretKey = new SecretKeySpec(sKeys, AESPARAM);
		} catch (final NoSuchAlgorithmException e) {
			Logger.getPathInstance().log(e.getMessage(), AESEncryption.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		}
	}

	/**
	 * This method is to encrypt plain text into cipher text.
	 *
	 * @param strToEncrypt
	 * @param secret
	 * @return cipher text as a String
	 */
	public static String encrypt(final String strToEncrypt, final String key) {
		try {
			setKey(key);
			final Cipher cipher = Cipher.getInstance(AES_ECB_PKCS_PADDING);
			cipher.init(Cipher.ENCRYPT_MODE, sEcretKey);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(UTF_ENCODING)));
		} catch (final Exception e) {
			Logger.getPathInstance().log(e.getMessage(), AESEncryption.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			System.out.println(ERROR_WHILE_ENCRYPTING + e.toString());
		}
		return null;
	}

	/**
	 * This method is to decrypt cipher text into plain text.
	 *
	 * @param strToDecrypt
	 * @param secret
	 * @return
	 */
	public static String decrypt(final String strToDecrypt, final String key) {
		try {
			setKey(key);
			final Cipher cipher = Cipher.getInstance(AES_ECB_PKCS_PADDING);
			cipher.init(Cipher.DECRYPT_MODE, sEcretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (final Exception e) {
			e.printStackTrace();
			Logger.getPathInstance().log(e.getMessage(), AESEncryption.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			System.out.println(ERROR_WHILE_DECRYPTING + e.toString());
		}
		return null;
	}
}
