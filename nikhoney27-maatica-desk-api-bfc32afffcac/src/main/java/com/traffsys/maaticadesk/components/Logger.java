package com.traffsys.maaticadesk.components;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;

import com.traffsys.maaticadesk.utils.AppConstant;
import com.traffsys.maaticadesk.utils.Util;

/**
 * @author Nikhil Sharma
 * @Date : 05-Jan-2018
 * @Description :Logger class is responsible for creating log base on Date with
 *              LogType[INFO,ERROR,DEBUG]
 */
public class Logger {
	private static Path rootLocation = null;

	public static Logger getPathInstance() {
		String s = System.getProperty("os.name");
		if (s.contains(AppConstant.WINDOW)) {
			rootLocation = Paths.get("./" + AppConstant.ROOT_DIRECTORY + "/");
		} else if (s.equalsIgnoreCase(AppConstant.LINUX)) {
			rootLocation = Paths.get("/home" + "/" + AppConstant.ROOT_DIRECTORY + "/");
		} else {
			rootLocation = Paths.get("./" + AppConstant.ROOT_DIRECTORY + "/");
		}
		return new Logger();
	}

	public void log(final String message, final String className, int levelType, int lineNumber) {
		switch (levelType) {
		case AppConstant.ERROR:
			printLogToFile(className, message, "ERROR", lineNumber);
			break;
		case AppConstant.INFO:
			printLogToFile(className, message, "INFO", lineNumber);
			break;
		case AppConstant.DEBUG:
			printLogToFile(className, message, "DEBUG", lineNumber);
			break;
		}
	}

	/**
	 * 
	 * @author Nikhil Sharma
	 * @Date : 05-Jan-2018
	 * @Description : This method is to write message into log file
	 * @Return : void
	 */
	public void printLogToFile(String className, String message, String logLevel, int lineNumber) {
		// write log info to file
		if (rootLocation != null) {
			File logFile = new File(rootLocation + "/" + AppConstant.WEB_ROOT_DIRECTORY + "/"
					+ AppConstant.LOG_DIRECTORY + "/" + Util.getCurrentDate("yyyy-MM-dd") + "maaticadesk.file");
			if (!logFile.exists()) {
				try {
					logFile.createNewFile();
					BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
					buf.append("Desk");
					buf.newLine();
					buf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			final Calendar c = Calendar.getInstance();
			int mYear = c.get(Calendar.YEAR);
			int mMonth = c.get(Calendar.MONTH);
			int mDay = c.get(Calendar.DAY_OF_MONTH);
			int mHour = c.get(Calendar.HOUR_OF_DAY);
			int mMinute = c.get(Calendar.MINUTE);
			int mSecond = c.get(Calendar.SECOND);
			String text = mDay + "/" + mMonth + "/" + mYear + " - " + mHour + " : " + mMinute + " : " + mSecond
					+ " :: [" + logLevel + "]:[" + className + "]:[" + lineNumber + "]" + message;
			try {
				// BufferedWriter for performance, true to set append to file flag
				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
				buf.append(text);
				buf.newLine();
				buf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
