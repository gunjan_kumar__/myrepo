/**
 * 
 */
package com.traffsys.maaticadesk.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.traffsys.maaticadesk.model.IssueType;
import com.traffsys.maaticadesk.model.RequestModel;
import com.traffsys.maaticadesk.model.ResponseModel;
import com.traffsys.maaticadesk.services.AddService;
import com.traffsys.maaticadesk.utils.AppConstant;
import com.traffsys.maaticadesk.utils.CheckTokkensUtility;
import com.traffsys.maaticadesk.utils.Util;


@RestController
public class AddController {
	
	@Autowired
	AddService addService;
	
	@Autowired
	CheckTokkensUtility mCheckTokkenUtility;
	
	@CrossOrigin
	@RequestMapping(value = "addissue", method = RequestMethod.POST)
	public ResponseModel issueType(@RequestBody RequestModel mRequestModel) {
		try {
			if (mCheckTokkenUtility.isValidTokkenKey(mRequestModel.getSessionTokken())) {
				IssueType savedIssue = addService.addIssue(mRequestModel);
				if(savedIssue != null && savedIssue.getIssueId() > 0) {
					return Util.setResponse(AppConstant.SUCCESS_STATUS_CODE, "SUCCESS", savedIssue);
				} else {
					return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Error while saving ticket", null);
				}
			} else {
				return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Unauthorized Access", null);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Bad Request", null);
		} catch (Exception e) {
			e.printStackTrace();
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Internal Server Error", null);
		}
		
	}
}
