package com.traffsys.maaticadesk.controllers;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.traffsys.maaticadesk.components.Logger;
import com.traffsys.maaticadesk.model.RequestModel;
import com.traffsys.maaticadesk.model.ResponseModel;
import com.traffsys.maaticadesk.services.UserService;
import com.traffsys.maaticadesk.utils.AppConstant;
import com.traffsys.maaticadesk.utils.CheckTokkensUtility;
import com.traffsys.maaticadesk.utils.Util;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	CheckTokkensUtility mCheckTokkenUtility;

	/**
	 * 
	 * @author A Kumar
	 * @Date : 18-Apr-2020
	 * @Description : This controller is to add user
	 * @Return : ResponseModel
	 */
	@RequestMapping(value = "addUser", method = RequestMethod.POST)
	public ResponseModel registerUser(@RequestBody RequestModel mRequestModel) {
		ResponseModel mResponseModel = null;
		try {
			if (mCheckTokkenUtility.isValidTokkenKey(mRequestModel.getSessionTokken())) {
				userService.addUser(mRequestModel.getData());
			} else {
				Logger.getPathInstance().log("Invalid access", UserController.class.getSimpleName(), AppConstant.ERROR,
						0);
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return mResponseModel;
	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public ResponseModel login(@RequestBody RequestModel mRequestModel) {
		try {
			return userService.doLoginService(mRequestModel);
		} catch (Exception e) {
			e.printStackTrace();
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Internal server error", null);
		}
	}
	
	@RequestMapping(value = "addRole", method = RequestMethod.POST)
	public ResponseModel addrole(@RequestBody RequestModel mRequestModel) {
		ResponseModel mResponseModel = null;
		try {
			if (mCheckTokkenUtility.isValidTokkenKey(mRequestModel.getSessionTokken())) {
				mResponseModel = userService.userRolemethod(mRequestModel);
			} else {
				Logger.getPathInstance().log("Invalid access", UserController.class.getSimpleName(), AppConstant.ERROR,
						0);
			}
		}catch(Exception e) {
			e.printStackTrace();
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Internal server error", null);
		}
		return mResponseModel;
	}
	
	@RequestMapping(value = "addDepartment", method = RequestMethod.POST)
	public ResponseModel addDepartment(@RequestBody RequestModel mRequestModel) {
		ResponseModel mResponseModel = null;
		try {
			if (mCheckTokkenUtility.isValidTokkenKey(mRequestModel.getSessionTokken())) {
				mResponseModel = userService.userDepartmentMethod(mRequestModel);
			} else {
				Logger.getPathInstance().log("Invalid access", UserController.class.getSimpleName(), AppConstant.ERROR,
						0);
			}
		}catch(Exception e) {
			e.printStackTrace();
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Internal server error", null);
		}
		return mResponseModel;
	}
	
	
	
}
