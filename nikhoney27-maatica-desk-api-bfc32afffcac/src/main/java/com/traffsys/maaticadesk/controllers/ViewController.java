package com.traffsys.maaticadesk.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.traffsys.maaticadesk.components.Logger;
import com.traffsys.maaticadesk.model.IssueType;
import com.traffsys.maaticadesk.model.RequestModel;
import com.traffsys.maaticadesk.model.ResponseModel;
import com.traffsys.maaticadesk.model.UserDetails;
import com.traffsys.maaticadesk.services.ViewService;
import com.traffsys.maaticadesk.utils.AppConstant;
import com.traffsys.maaticadesk.utils.CheckTokkensUtility;
import com.traffsys.maaticadesk.utils.Util;

@RestController
public class ViewController {

	@Autowired
	ViewService viewService;
	
	@Autowired
	CheckTokkensUtility mCheckTokkenUtility;
	
	@CrossOrigin
	@PostMapping(value="users")
	public ResponseModel getAllUser(@RequestBody RequestModel mRequestModel) {
		try {
			if (mCheckTokkenUtility.isValidTokkenKey(mRequestModel.getSessionTokken())) {
				List<UserDetails> users = viewService.getAllUsers();
				if(users != null && users.size() > 0) {
					return Util.setResponse(AppConstant.SUCCESS_STATUS_CODE, "SUCCESS", users);
				} else {
					return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "No Record found", null);
				}
			} else {
				return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Not a valid user", null);
			}
		} catch (Exception e) {
			Logger.getPathInstance().log(e.getMessage(), ViewController.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Internal Server Error", null);
		}		
	}
	
	
	@CrossOrigin
	@PostMapping(value="issues")
	public ResponseModel getIssues(@RequestBody RequestModel mRequestModel) {
		try {
			if (mCheckTokkenUtility.isValidTokkenKey(mRequestModel.getSessionTokken())) {
				List<IssueType> issues = viewService.getIssues();
				if(issues != null && issues.size() > 0) {
					return Util.setResponse(AppConstant.SUCCESS_STATUS_CODE, "SUCCESS", issues);
				} else {
					return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "No Record found", null);
				}
			} else {
				return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Not a valid user", null);
			}
		} catch (Exception e) {
			Logger.getPathInstance().log(e.getMessage(), ViewController.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Internal Server Error", null);
		}		
	}
}
