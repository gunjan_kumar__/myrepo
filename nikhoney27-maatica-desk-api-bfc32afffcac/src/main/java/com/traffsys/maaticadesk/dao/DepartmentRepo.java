package com.traffsys.maaticadesk.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.traffsys.maaticadesk.model.Department;

public interface DepartmentRepo extends JpaRepository<Department , Long>{

}
