package com.traffsys.maaticadesk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.traffsys.maaticadesk.model.IssueType;

@Repository
public interface IssueTypeRepo extends JpaRepository<IssueType, Long>{

}
