package com.traffsys.maaticadesk.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.traffsys.maaticadesk.model.ResponseModel;
import com.traffsys.maaticadesk.model.UserDetails;
import com.traffsys.maaticadesk.utils.AppConstant;

@Component
public class RegistrationDAO {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	private SessionFactory getSessionFactory() {
		return entityManagerFactory.unwrap(SessionFactory.class);
	}

 
	public ResponseModel saveUserRegistrationDetailsToDB(UserDetails mDetails) {
		ResponseModel mRespose = new ResponseModel();
		SessionFactory mSessionFactory = getSessionFactory();
		if (mSessionFactory != null) {
			final Session mSession = mSessionFactory.openSession();
		 
			try {

				Transaction tx = mSession.beginTransaction();
				mSession.save(mDetails);
				tx.commit();
				mSession.close();
 

				mRespose.setMessage("Added successfully to Database");
				mRespose.setStatus(AppConstant.SUCCESS_STATUS_CODE);
				List<HashMap<String, Object>> mResponseList = new ArrayList<HashMap<String, Object>>();
				HashMap<String, Object> hashMap = new HashMap<>();
				hashMap.put("userId", mDetails.getUserId());
				mResponseList.add(hashMap);

				mRespose.setMyObjectList(mResponseList);

				return mRespose;
			} catch (Exception e) {
				e.printStackTrace();
				 
				throw new RuntimeException("Duplicate Error!!");

			} finally {
				if (mSession.isOpen()) {
					mSession.close();
				} else {
					 
				}
			}
		} else {
 			mRespose.setMessage("Server Error ");
			mRespose.setStatus(AppConstant.ERROR_STATUS_CODE);
			return mRespose;
		}
	}

 
}