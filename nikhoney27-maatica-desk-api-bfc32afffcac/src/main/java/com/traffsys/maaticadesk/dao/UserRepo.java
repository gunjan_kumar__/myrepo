package com.traffsys.maaticadesk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.traffsys.maaticadesk.model.UserDetails;
import com.traffsys.maaticadesk.model.UserLoginStatus;

@Repository
public interface UserRepo extends JpaRepository<UserDetails, Long>{

	UserLoginStatus save(UserLoginStatus loginStatus);

	UserDetails findByUsername(String username);

}
