package com.traffsys.maaticadesk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.traffsys.maaticadesk.model.UserRole;

@Repository
public interface UserRoleRepo extends JpaRepository<UserRole, Long>{
	
	

}
