package com.traffsys.maaticadesk.dao;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.apache.xmlbeans.impl.xb.xsdschema.RestrictionDocument.Restriction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.traffsys.maaticadesk.model.ResponseModel;
import com.traffsys.maaticadesk.model.UserDetails;
import com.traffsys.maaticadesk.model.UserLoginStatus;
import com.traffsys.maaticadesk.utils.AppConstant;

@Component
public class User_login_dao {
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	@Autowired
	User_status_repo mStatusRepo;
	
	@Autowired 
	UserRepo userRepo;

/**
 * 	
 * @param user_id
 * used to check if a user is active or not.
 * @return
 */
	public boolean checkUserStatus(long user_id) {
		boolean response = false;
		UserDetails user = userRepo.findById(user_id).get();
		
		if(user != null) {
			UserLoginStatus userStatus = user.getmLoginStatus();
			if(userStatus != null) {
				if(userStatus.getPasswordAttempts() > 0) {
					if(userStatus.getUserStatus() == 1) {
						response = true;
					} else {
						throw new RuntimeException("User is not active");
					}
				} else {
					throw new RuntimeException("Password attemt exceded");
				}
			} else {
				System.err.println("Admin may be");
			}
		} else {
			throw new RuntimeException("User not exist");
		}
		return response;
	}
	
	
	public UserLoginStatus updateUserAttempts(UserLoginStatus loginStatus) {

		loginStatus = userRepo.save(loginStatus);
		return loginStatus;
	}
	
//	private SessionFactory getSessionFactory() {
//		return entityManagerFactory.unwrap(SessionFactory.class);
//	}
//	 
//	public ResponseModel saveUserLoginStatusToDB(UserLoginStatus mLoginStatus){
//		ResponseModel mRespose = new ResponseModel();
//		SessionFactory mSessionFactory = getSessionFactory();
//		if(mSessionFactory != null) {
//			final Session mSession = mSessionFactory.openSession();
//			
//			try {
//				Transaction tx = mSession.beginTransaction();
//				mSession.save(mLoginStatus);
//				
////				List<UserDetails> users = mSession.createCriteria(UserDetails.class).add(Restrictions.eq("userId", 1)).list();
////				users.get(0);
//				
//				tx.commit();
//				mSession.close();
//				
//				mRespose.setMessage("Added successfully to Database");
//				mRespose.setStatus(AppConstant.SUCCESS_STATUS_CODE);
//				
//			} catch (Exception e) {
//				e.printStackTrace();
//			} finally {
//				if(mSession != null && mSession.isOpen()) {
//					mSession.close();
//				}
//
//			}
//		}
//		return mRespose;
		
	}
