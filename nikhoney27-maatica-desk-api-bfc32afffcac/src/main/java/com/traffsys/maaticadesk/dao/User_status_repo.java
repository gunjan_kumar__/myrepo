package com.traffsys.maaticadesk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.traffsys.maaticadesk.model.UserLoginStatus;

@Repository
public interface User_status_repo extends JpaRepository<UserLoginStatus, Long>{
	
}
