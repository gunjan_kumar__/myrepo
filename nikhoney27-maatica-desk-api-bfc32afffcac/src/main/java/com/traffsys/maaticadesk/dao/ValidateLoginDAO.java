package com.traffsys.maaticadesk.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.traffsys.maaticadesk.components.Logger;
import com.traffsys.maaticadesk.model.UserDetails;
import com.traffsys.maaticadesk.utils.AppConstant;

@Component
@Transactional
public class ValidateLoginDAO {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	private SessionFactory getSessionFactory() {
		return entityManagerFactory.unwrap(SessionFactory.class);
	}

	/**
	 * @author Nikhil Sharma
	 * @Date : 20-Jan-2020
	 * @Description : This method is to check valid tokken key
	 * @Return : Boolean
	 */
	public Boolean getValidTokken(String tokkenKey) {
		boolean isTokkenValid = false;

		Session mSession = null;
		try {
			SessionFactory mSessionFactory = getSessionFactory();
			if (mSessionFactory != null) {
				mSession = mSessionFactory.openSession();

				if (mSession != null) {

					Criteria mCriteria = mSession.createCriteria(UserDetails.class);
					mCriteria.add(Restrictions.eq("tokkenKey", tokkenKey));
					List<UserDetails> mDetails = mCriteria.list();

					if (mDetails != null && mDetails.size() > 0) {
						isTokkenValid = true;
					}
				} else {
					Logger.getPathInstance().log("Session is null", ValidateLoginDAO.class.getSimpleName(),
							AppConstant.INFO, 0);
				}
			} else {
				Logger.getPathInstance().log("SessionFactory is null", ValidateLoginDAO.class.getSimpleName(),
						AppConstant.INFO, 0);
			}
		} catch (Exception e) {
			Logger.getPathInstance().log(e.getMessage(), ValidateLoginDAO.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
		} finally {
			if (mSession != null) {
				if (mSession.isOpen()) {
					mSession.clear();
					if (mSession.isConnected())
						mSession.close();
				}
			}
		}
		return isTokkenValid;

	}
}
