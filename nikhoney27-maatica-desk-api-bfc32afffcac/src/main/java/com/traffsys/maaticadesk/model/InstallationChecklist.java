package com.traffsys.maaticadesk.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_installation_checklist")
public class InstallationChecklist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "checklist_id", nullable = false)
	private long checklistId;

	@Column(name = "checklist_name", nullable = false, unique = true)
	private String checklistName;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "creationDate", nullable = true)
	private Date creationDate;

	public long getChecklistId() {
		return checklistId;
	}

	public void setChecklistId(long checklistId) {
		this.checklistId = checklistId;
	}

	public String getChecklistName() {
		return checklistName;
	}

	public void setChecklistName(String checklistName) {
		this.checklistName = checklistName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public SystemType getmSystemType() {
		return mSystemType;
	}

	public void setmSystemType(SystemType mSystemType) {
		this.mSystemType = mSystemType;
	}

	@Column(name = "status", nullable = true)
	private Integer status;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "checklist_system_fk")
	private SystemType mSystemType;

}
