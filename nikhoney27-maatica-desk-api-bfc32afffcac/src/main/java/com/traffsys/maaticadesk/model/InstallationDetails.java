package com.traffsys.maaticadesk.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "installation_details")
public class InstallationDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "installation_id", nullable = false)
	private long installationId;

	@Column(name = "installation_name", nullable = false, unique = true)
	private String installationName;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "creation_Date", nullable = true)
	private Date creationDate;

	@Column(name = "completion_Date", nullable = true)
	private Date completionDate;

	@Column(name = "status", nullable = true)
	private Integer status;

	@Column(name = "location", nullable = true)
	private String location;

	@Column(name = "checklist_description", nullable = true)
	private String checklistDescription;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "install_system_fk")
	private SystemType mSystemType;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "install_project_fk")
	private Project mProject;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "userId_fk")
	private UserDetails mUserDetails;

	public long getInstallationId() {
		return installationId;
	}

	public void setInstallationId(long installationId) {
		this.installationId = installationId;
	}

	public String getInstallationName() {
		return installationName;
	}

	public void setInstallationName(String installationName) {
		this.installationName = installationName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getChecklistDescription() {
		return checklistDescription;
	}

	public void setChecklistDescription(String checklistDescription) {
		this.checklistDescription = checklistDescription;
	}

	public SystemType getmSystemType() {
		return mSystemType;
	}

	public void setmSystemType(SystemType mSystemType) {
		this.mSystemType = mSystemType;
	}

	public Project getMpProject() {
		return mProject;
	}

	public void setMpProject(Project mProject) {
		this.mProject = mProject;
	}

	public UserDetails getmUserDetails() {
		return mUserDetails;
	}

	public void setmUserDetails(UserDetails mUserDetails) {
		this.mUserDetails = mUserDetails;
	}

}
