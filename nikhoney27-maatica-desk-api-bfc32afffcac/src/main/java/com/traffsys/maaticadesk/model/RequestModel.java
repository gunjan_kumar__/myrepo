/**
 * 
 */
package com.traffsys.maaticadesk.model;

/**
 * @author @Ashok@
 * @Date : 12-Sep-2018
 * @Description :to hold the client request.
 */
public class RequestModel {
	
	private String data;
	private String username;
	private String sessionTokken;
	private String basicAuth;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionTokken() {
		return sessionTokken;
	}

	public void setSessionTokken(String sessionTokken) {
		this.sessionTokken = sessionTokken;
	}

	public String getBasicAuth() {
		return basicAuth;
	}

	public void setBasicAuth(String basicAuth) {
		this.basicAuth = basicAuth;
	}

}
