/**
 * 
 */
package com.traffsys.maaticadesk.model;

/**
 * @author Nikhil Sharma
 * @Date : 14-Jan-2020
 * @Description :
 */
public class ResponseModelv2 {
	private Integer status;
	private String message;

	public long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

	private long totalPages;
	private long totalRecord;

	public long getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(long totalRecord) {
		this.totalRecord = totalRecord;
	}

	private Object myObjectList;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getMyObjectList() {
		return myObjectList;
	}

	public void setMyObjectList(Object myObjectList) {
		this.myObjectList = myObjectList;
	}

	@Override
	public String toString() {
		return "USERLOG:[status:" + status + ",message:" + message + "]";
	}

}
