package com.traffsys.maaticadesk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_systemTpe")
public class SystemType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "system_id", nullable = false)
	private long systemId;

	@Column(name = "system_type", nullable = false )
	private String systemType;

	@Column(name = "description", nullable = true)
	private String description;

	public long getSystemId() {
		return systemId;
	}

	public void setSystemId(long systemId) {
		this.systemId = systemId;
	}

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
