package com.traffsys.maaticadesk.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ticket_details")
public class TicketDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ticket_id", nullable = false)
	private long ticketId;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "system_type_fk")
	private SystemType mSystemType;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "project_fk")
	private Project mProject;

	public long getTicketId() {
		return ticketId;
	}

	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}

	public SystemType getmSystemType() {
		return mSystemType;
	}

	public void setmSystemType(SystemType mSystemType) {
		this.mSystemType = mSystemType;
	}

	public Project getmProject() {
		return mProject;
	}

	public void setmProject(Project mProject) {
		this.mProject = mProject;
	}

	public UserDetails getmUserDetails() {
		return mUserDetails;
	}

	public void setmUserDetails(UserDetails mUserDetails) {
		this.mUserDetails = mUserDetails;
	}

	public IssueType getmIssueType() {
		return mIssueType;
	}

	public void setmIssueType(IssueType mIssueType) {
		this.mIssueType = mIssueType;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public Integer getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(Integer ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id_fk")
	private UserDetails mUserDetails;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "issue_type_fk")
	private IssueType mIssueType;

	@Column(name = "creation_Date", nullable = true)
	private Date creationDate;

	@Column(name = "completion_Date", nullable = true)
	private Date completionDate;

	@Column(name = "ticket_status", nullable = true)
	private Integer ticketStatus;

	@Column(name = "serial_number", nullable = true)
	private String serialNumber;

	@Column(name = "location", nullable = true)
	private String location;

	@Column(name = "latitude", nullable = true)
	private String latitude;

	@Column(name = "longitude", nullable = true)
	private String longitude;

	@Column(name = "remarks", nullable = true)
	private String remarks;

}
