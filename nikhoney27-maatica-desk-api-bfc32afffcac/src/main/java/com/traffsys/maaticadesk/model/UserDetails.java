package com.traffsys.maaticadesk.model;

 

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

 
@Entity
@Table(name = "user_details")
public class UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", nullable = false)
	private long userId;

	@Column(name = "username" , unique = true)
	private String username;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "contact_no")
	private String contactNo;

	@Column(name = "registration_date")
	private Timestamp registrationDate;

	@Column(name = "device_id")
	private String deviceId;
	
	@Column(name = "date_of_birth")
	private Timestamp dateOfBirth;
	
	public Timestamp getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Timestamp dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Column(name = "status")
	private Integer status;

	@Column(name = "fcm_tokken")
	private String fcmTokken;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "tokken_key")
	private String tokkenKey;
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

 

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFcmTokken() {
		return fcmTokken;
	}

	public void setFcmTokken(String fcmTokken) {
		this.fcmTokken = fcmTokken;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTokkenKey() {
		return tokkenKey;
	}

	public void setTokkenKey(String tokkenKey) {
		this.tokkenKey = tokkenKey;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public UserRole getmUserRole() {
		return mUserRole;
	}

	public void setmUserRole(UserRole mUserRole) {
		this.mUserRole = mUserRole;
	}

	public Department getmDepartment() {
		return mDepartment;
	}

	public void setmDepartment(Department mDepartment) {
		this.mDepartment = mDepartment;
	}

	public UserLoginStatus getmLoginStatus() {
		return mLoginStatus;
	}

	public void setmLoginStatus(UserLoginStatus mLoginStatus) {
		this.mLoginStatus = mLoginStatus;
	}

	@Column(name = "user_type")
	private Integer userType;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "m_role_fk")
	private UserRole mUserRole;
 
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "m_department_fk")
	private Department mDepartment;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "m_loginStatus_fk")
	private UserLoginStatus mLoginStatus;
 
}
