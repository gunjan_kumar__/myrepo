package com.traffsys.maaticadesk.model;

 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

 
@Entity
@Table(name = "m_user_login_status")
public class UserLoginStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_status_id", nullable = false)
	private long userStatusId;
 
	
	@Column(name = "password_attempts")
	private Integer passwordAttempts;
	
	@Column(name = "user_status")
	private Integer userStatus;

	public long getUserStatusId() {
		return userStatusId;
	}

	public void setUserStatusId(long userStatusId) {
		this.userStatusId = userStatusId;
	}

	public Integer getPasswordAttempts() {
		return passwordAttempts;
	}

	public void setPasswordAttempts(Integer passwordAttempts) {
		this.passwordAttempts = passwordAttempts;
	}

	public Integer getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}
	
 
 
 
}
