package com.traffsys.maaticadesk.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.traffsys.maaticadesk.dao.IssueTypeRepo;
import com.traffsys.maaticadesk.model.IssueType;
import com.traffsys.maaticadesk.model.RequestModel;

@Service
public class AddService {

	@Autowired
	IssueTypeRepo issueTypeRepo;
	
	public IssueType addIssue(RequestModel mRequestModel) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		IssueType issue = mapper.readValue(mRequestModel.getData(), IssueType.class);
		issue.setCreationDate(new Date());
		return issueTypeRepo.save(issue);
	}

}
