package com.traffsys.maaticadesk.services;

import java.sql.Timestamp;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.traffsys.maaticadesk.dao.DepartmentRepo;
import com.traffsys.maaticadesk.dao.RegistrationDAO;
import com.traffsys.maaticadesk.dao.UserRepo;
import com.traffsys.maaticadesk.dao.UserRoleRepo;
import com.traffsys.maaticadesk.dao.User_login_dao;
import com.traffsys.maaticadesk.model.Department;
import com.traffsys.maaticadesk.model.RequestModel;
import com.traffsys.maaticadesk.model.ResponseModel;
import com.traffsys.maaticadesk.model.UserDetails;
import com.traffsys.maaticadesk.model.UserLoginStatus;
import com.traffsys.maaticadesk.model.UserRole;
import com.traffsys.maaticadesk.utils.AppConstant;
import com.traffsys.maaticadesk.utils.Util;

@Service
public class UserService {

	@Autowired
	RegistrationDAO mRegistrationDAO;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	User_login_dao userDao;

	@Autowired
	UserRoleRepo userRoleRepo;
	
	@Autowired
	DepartmentRepo departmentRepo;
	
	public ResponseModel addUser(final String data) {
		ResponseModel mResponseModel = null;
		try {
			if (data != null) {
				if (!data.isEmpty()) {

					UserDetails mDetails = setDataToModel(data);
					if (mDetails != null) {
						mResponseModel = mRegistrationDAO.saveUserRegistrationDetailsToDB(mDetails);
					} else {
						System.err.println("ERROR while parsing data and setting into model");
						mResponseModel = Util.setResponse(AppConstant.ERROR_STATUS_CODE,
								"Error while creating new user", null);
					}
				} else {
					mResponseModel = Util.setResponse(AppConstant.ERROR_STATUS_CODE,
							"Required parameter data is not present", null);
				}
			} else {
				mResponseModel = Util.setResponse(AppConstant.ERROR_STATUS_CODE,
						"Required parameter data is not present", null);
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (e.getMessage().contains("Duplicate Error")) {
				mResponseModel = Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Already Registered", null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mResponseModel;
	}

	private UserDetails setDataToModel(final String data) {
		UserDetails mUserDetails = null;
		try {
			JSONObject mJsonObject = new JSONObject(data);
			mUserDetails = new UserDetails();

			mUserDetails.setFirstName(mJsonObject.optString("firstName"));
			mUserDetails.setMiddleName(mJsonObject.optString("middleName"));
			mUserDetails.setLastName(mJsonObject.optString("lastName"));
			mUserDetails.setContactNo(mJsonObject.optString("contactNo"));
			mUserDetails.setEmailId(mJsonObject.optString("emailId"));
			mUserDetails.setDeviceId(mJsonObject.optString("deviceId"));
			mUserDetails.setFcmTokken(mJsonObject.optString("fcmTokken"));
			mUserDetails.setRegistrationDate(Util.getCurrentSQLTimeStamp());
			mUserDetails.setTokkenKey(mJsonObject.optString("tokkenKey"));
			mUserDetails.setUsername(mJsonObject.optString("username").trim());
			mUserDetails.setPassword(mJsonObject.optString("password"));

			// DEPARTMENT
			JSONObject mDepartmentJSON = new JSONObject(mJsonObject.getString("mDepartment"));
			Department mDepartment = new Department();
			mDepartment.setDepartmentName(mDepartmentJSON.getString("departmentName"));
			mDepartment.setDescription(mDepartmentJSON.getString("description"));
			mDepartment.setDepartmentId(mDepartmentJSON.getLong("departmentId"));
			mUserDetails.setmDepartment(mDepartment);

			// LOGIN STATUS
			UserLoginStatus mUserLoginStatus = new UserLoginStatus();
			mUserLoginStatus.setPasswordAttempts(7);
			mUserLoginStatus.setUserStatus(1);
			mUserDetails.setmLoginStatus(mUserLoginStatus);

			// ROLE
			UserRole mUserRole = new UserRole();
			mUserRole.setRole(mJsonObject.optString("role"));
			mUserRole.setRoleId(mJsonObject.optLong("roleId"));
			mUserRole.setStatus(mJsonObject.optInt("status"));
			mUserRole.setDescription(mJsonObject.optString("description"));
			mUserDetails.setmUserRole(mUserRole);

			// USERTYPE
			if (mJsonObject.has("userType")) {
				String user_type = mJsonObject.getString("userType");
				if (user_type.equalsIgnoreCase("web")) {
					mUserDetails.setUserType(1);
				} else if (user_type.equalsIgnoreCase("mobile")) {
					mUserDetails.setUserType(2);
				}
			}

			if (mJsonObject.getString("dateOfBirth") != null && !mJsonObject.getString("dateOfBirth").isEmpty()) {
				Timestamp date_of_birth = Util.stringToTimestamp(mJsonObject.optString("dateOfBirth"), "yyyy-MM-dd");
				mUserDetails.setDateOfBirth(date_of_birth);
			} else {
				mUserDetails.setDateOfBirth(null);
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return mUserDetails;
	}

	/**
	 * @author Nikhil Sharma
	 * @Date : 23-Apr-2020
	 * @Description :
	 * @Return : void
	 */

	public ResponseModel doLoginService(final RequestModel mRequestModel) {
		// String decryptData = Util.decryptString(mRequestModel.getData(),
		// AppConstant.AES_KEY);
		JSONObject mJsonObject = new JSONObject(mRequestModel.getData());
		String username = mJsonObject.getString("username");
		String password = mJsonObject.getString("password");

		UserDetails mUserDetails = userRepo.findByUsername(username.trim());
		if (mUserDetails == null)
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "User not exist", null);
		if (!mUserDetails.getPassword().equals(password.trim())) {
			UserLoginStatus attempts = mUserDetails.getmLoginStatus();
			if (attempts.getPasswordAttempts() > 0) {
				attempts.setPasswordAttempts(attempts.getPasswordAttempts() - 1);
				userDao.updateUserAttempts(attempts);
			}
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Incoreect password", null);
			// DECREMENT PASSWORD ATTEMP
		}
//		if(!user.getUsername().equals("superadmin"))
//			if(!userDao.checkUserStatus(user.getUserId()))
//				return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Inactive user", null);
//		
		return Util.setResponse(AppConstant.SUCCESS_STATUS_CODE, "Success", mUserDetails);

	}
/**
 * to save role into database.
 * @param mRequestModel
 * @return
 */
	public ResponseModel userRolemethod(final RequestModel mRequestModel) {
		JSONObject mJsonObject = new JSONObject(mRequestModel.getData());
		// ROLE
		UserRole mUserRole = new UserRole();
		mUserRole.setRole(mJsonObject.optString("role"));
		//mUserRole.setRoleId(mJsonObject.optLong("roleId"));
		mUserRole.setStatus(mJsonObject.optInt("status"));
		mUserRole.setDescription(mJsonObject.optString("description"));
		mUserRole.setCreationDate(Util.getCurrentDateTime());
		mUserRole = userRoleRepo.save(mUserRole);
		
		if(mUserRole != null && mUserRole.getRoleId() > 0) {
			return Util.setResponse(AppConstant.SUCCESS_STATUS_CODE, AppConstant.SUCCESS_API_MESSGAE, mUserRole);
		} else {
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Record not found", null);
		}
	}

    public ResponseModel userDepartmentMethod(final RequestModel mRequestModel) {
    	JSONObject mJsonObject = new JSONObject(mRequestModel.getData());
    	Department mDepartment = new Department();
    	mDepartment.setDepartmentName(mJsonObject.getString("departmentName"));
		mDepartment.setDescription(mJsonObject.getString("description"));
		//mDepartment.setDepartmentId(mJsonObject.getLong("departmentId"));
		mDepartment.setCreationDate(Util.getCurrentDateTime());
		mDepartment = departmentRepo.save(mDepartment);
		if(mDepartment != null && mDepartment.getDepartmentId() > 0) {
			return Util.setResponse(AppConstant.SUCCESS_STATUS_CODE, AppConstant.SUCCESS_API_MESSGAE, mDepartment);
		}else {
			return Util.setResponse(AppConstant.ERROR_STATUS_CODE, "Record not found", null);
		}
    }
}
