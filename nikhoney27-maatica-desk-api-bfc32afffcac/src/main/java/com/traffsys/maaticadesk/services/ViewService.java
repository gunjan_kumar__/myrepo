package com.traffsys.maaticadesk.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.traffsys.maaticadesk.dao.IssueTypeRepo;
import com.traffsys.maaticadesk.dao.UserRepo;
import com.traffsys.maaticadesk.model.IssueType;
import com.traffsys.maaticadesk.model.UserDetails;

@Service
public class ViewService {

	@Autowired
	UserRepo userRepo;
	
	@Autowired
	IssueTypeRepo issueType;
	
	public List<UserDetails> getAllUsers() {
		return userRepo.findAll();
	}

	public UserDetails getUserById(long userId) {
		return userRepo.findById(userId).get();
	}

	public List<IssueType> getIssues() {
		return issueType.findAll();
	}

}
