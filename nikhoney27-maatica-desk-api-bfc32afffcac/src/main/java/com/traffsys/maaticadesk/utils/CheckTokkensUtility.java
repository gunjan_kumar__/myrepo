package com.traffsys.maaticadesk.utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.traffsys.maaticadesk.dao.ValidateLoginDAO;
import com.traffsys.maaticadesk.model.RequestModel;
import com.traffsys.maaticadesk.model.UserDetails;



@Component
public class CheckTokkensUtility {

	@Autowired
	ValidateLoginDAO mValidateLoginDAO;


	public static boolean checkRequestModelOnPost(RequestModel mRequestModel) {
		if (mRequestModel != null) {
			if (mRequestModel.getBasicAuth() != null && !mRequestModel.getBasicAuth().isEmpty()) {
				if (mRequestModel.getData() != null && !mRequestModel.getData().isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}



	/**
	 * 
	 * @author Nikhil Sharma
	 * @Date : 23-Apr-2020
	 * @Description :
	 * @Return : boolean
	 */
	public boolean isValidTokkenKey(final String tokkenKey) throws Exception {
		boolean isValidAuth = false;
		if (tokkenKey != null) {
			if (!tokkenKey.isEmpty()) {
				isValidAuth = mValidateLoginDAO.getValidTokken(tokkenKey);
			}
		}
		return isValidAuth;
	}

	

	
}
