package com.traffsys.maaticadesk.utils;
/**
 * 
 * @author Ashok kumar
 * Network Respnse Class
 * Holds all network response code
 */

public class NetworkResponse {
	public static String networkResponse(int statusCode) {
		 String netResponseCode = null;
		switch(statusCode) {
//1xx Informational responses
		case 100 :{
			netResponseCode = "Continue";
			break;
		}
		case 101 :{
			netResponseCode = "Switching Protocols";
			break;
		}

		case 102 :{
			netResponseCode = "Processing";
			break;
		}

		case 103 :{
			netResponseCode = "Early Hints";
			break;
		}

		case 200 :{
			netResponseCode = "OK";
			break;
		}
		case 201 :{
			netResponseCode = "Created";
			break;
		}
		case 202 :{
			netResponseCode = "Accepted";
			break;
		}
		case 203  :{
			netResponseCode = "Non-Authoritative Information";
			break;
		}
		case 204  :{
			netResponseCode = "No Content";
			break;
		}
		case 205  :{
			netResponseCode = "Reset Content";
			break;
		}
		case 206  :{
			netResponseCode = "Partial Content";
			break;
		}
		case 207  :{
			netResponseCode = "Multi-Status";
			break;
		}
		case 208  :{
			netResponseCode = "Already Reported";
			break;
		}
		case 226  :{
			netResponseCode = "IM Used";
			break;
		}
//3xx Redirection
		case 300 :{
			netResponseCode = "Multiple Choices";
			break;
		}
		case 301 :{
			netResponseCode = "Moved Permanently";
			break;
		}
		case 302 :{
			netResponseCode = "Found";
			break;
		}
		case 303  :{
			netResponseCode = "See Other";
			break;
		}
		case 304  :{
			netResponseCode = "Not Modified";
			break;
		}
		case 305  :{
			netResponseCode = "Use Proxy";
			break;
		}
		case 306  :{
			netResponseCode = "Switch Proxy";
			break;
		}
		case 307  :{
			netResponseCode = "Temporary Redirect";
			break;
		}
		case 308  :{
			netResponseCode = "Permanent Redirect";
			break;
		}
//4xx Client errors	
		case 400 :{
			netResponseCode = "Bad Request";
			break;
		}
		case 401 :{
			netResponseCode = "Unauthorized";
			break;
		}
		case 402 :{
			netResponseCode = "Payment Required";
			break;
		}
		case 403  :{
			netResponseCode = "Forbidden";
			break;
		}
		case 404  :{
			netResponseCode = "Not Found";
			break;
		}
		case 405  :{
			netResponseCode = "Method Not Allowed";
			break;
		}
		case 406  :{
			netResponseCode = "Not Acceptable";
			break;
		}
		case 407  :{
			netResponseCode = "Proxy Authentication Required";
			break;
		}
		case 408  :{
			netResponseCode = "Request Timeout";
			break;
		}

		case 409 :{
			netResponseCode = "Conflict";
			break;
		}
		case 410 :{
			netResponseCode = "Gone";
			break;
		}
		case 411 :{
			netResponseCode = "Length Required";
			break;
		}
		case 412  :{
			netResponseCode = "Precondition Failed";
			break;
		}
		case 413  :{
			netResponseCode = "Payload Too Large";
			break;
		}
		case 414  :{
			netResponseCode = "URI Too Long";
			break;
		}
		case 415  :{
			netResponseCode = "Unsupported Media Type";
			break;
		}
		case 416  :{
			netResponseCode = "Range Not Satisfiable";
			break;
		}
		case 417  :{
			netResponseCode = "Expectation Failed";
			break;
		}

		case 418 :{
			netResponseCode = "I am a teapot";
			break;
		}
		case 421 :{
			netResponseCode = "Misdirected Request";
			break;
		}
		case 422 :{
			netResponseCode = "Unprocessable Entity";
			break;
		}
		case 423  :{
			netResponseCode = "Locked";
			break;
		}
		case 424  :{
			netResponseCode = "Failed Dependency";
			break;
		}
		case 426  :{
			netResponseCode = "Upgrade Required";
			break;
		}
		case 428  :{
			netResponseCode = "Precondition Required";
			break;
		}
		case 429  :{
			netResponseCode = "Too Many Requests";
			break;
		}
		case 431  :{
			netResponseCode = "Request Header Fields Too Large";
			break;
		}

		case 451  :{
			netResponseCode = "Unavailable For Legal Reasons";
			break;
		}
//5xx Server errors
		case 500 :{
			netResponseCode = "Internal Server Error";
			break;
		}
		case 501 :{
			netResponseCode = "Not Implemented";
			break;
		}
		case 502 :{
			netResponseCode = "Bad Gateway";
			break;
		}
		case 503  :{
			netResponseCode = "Service Unavailable";
			break;
		}
		case 504  :{
			netResponseCode = "Gateway Timeout";
			break;
		}
		case 505  :{
			netResponseCode = "HTTP Version Not Supported";
			break;
		}
		case 506  :{
			netResponseCode = "Variant Also Negotiates";
			break;
		}
		case 507  :{
			netResponseCode = "Insufficient Storage";
			break;
		}
		case 508  :{
			netResponseCode = "Loop Detected";
			break;
		}
		case 510 :{
			netResponseCode = "Not Extended";
			break;
		}
		case 511 :{
			netResponseCode = "Network Authentication Required";
			break;
		}
//Cloudflare
		case 520 :{
			netResponseCode = "Unknown Error";
			break;
		}
		case 521 :{
			netResponseCode = "Web Server Is Down";
			break;
		}
		case 522 :{
			netResponseCode = "Connection Timed Out";
			break;
		}
		case 523  :{
			netResponseCode = "Origin Is Unreachable";
			break;
		}
		case 524  :{
			netResponseCode = "A Timeout Occurred";
			break;
		}
		case 525  :{
			netResponseCode = "SSL Handshake Failed";
			break;
		}
		case 526  :{
			netResponseCode = "Invalid SSL Certificate";
			break;
		}
		case 527  :{
			netResponseCode = "Railgun Error";
			break;
		}
		}
		
		return netResponseCode;
	}
}
