package com.traffsys.maaticadesk.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.traffsys.maaticadesk.components.AESEncryption;
import com.traffsys.maaticadesk.components.Base64;
import com.traffsys.maaticadesk.components.Logger;
import com.traffsys.maaticadesk.model.ResponseModel;
import com.traffsys.maaticadesk.model.ResponseModelv2;


public class Util {

	public static final String SECURITY_KEY = "98766543210";
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	public static final String AES_ECB_PKCS5_PADDING = "AES/ECB/PKCS5Padding";
	public static final String UTF_8 = "UTF-8";

	/**
	 * 
	 * @author Ashok@
	 * @Date : Jan 13, 2018
	 * @Description :Method to get current date and time i.e. yyyy/MM/dd HH:mm:ss
	 *              format
	 * @Return : Date
	 */
	public static Date getCurrentDateTime() {
		// DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return date;
	}

	/**
	 * 
	 * @author Nikhil Sharma
	 * @Date : 28-Feb-2020
	 * @Description :
	 * @Return : String
	 */
	public static String encryptString(String pass, String key) {
		if (pass != null) {
			return AESEncryption.encrypt(pass, key);
		} else {
			throw new NullPointerException();
		}
	}

	// TODO Remove
	public static Date getCurrentUtilDate(final String date_formate) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(date_formate);
		/* DateTimeFormatter dtf = DateTimeFormatter.ofPattern(date_formate); */
		DateFormat format = new SimpleDateFormat(date_formate, Locale.ENGLISH);
		Date date = new Date();
		// System.out.println(formatter.format(date));
		return format.parse(formatter.format(date));
	}



	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 12, 2018
	 * @Description :to get current/today date.
	 * @Return : Date
	 */
	public static String getCurrentDate(final String date_format) {
		DateFormat dateFormat = new SimpleDateFormat(date_format);
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static Date getStringToUtilDate(final String date, final String date_formate) throws ParseException {
		// DateFormat formatter = new SimpleDateFormat(date_formate);
		SimpleDateFormat formatter = new SimpleDateFormat(date_formate);
		Date date1 = formatter.parse(date);
		return date1;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Oct 1, 2018
	 * @Description :used to convert String date into SQL date in given date.
	 * @Return : java.sql.Date
	 */
	public static java.sql.Date getStringToSQLDate(final String date, final String date_formate) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat(date_formate);
		java.util.Date udate = sdf1.parse(date);
		java.sql.Date sqlDate = new java.sql.Date(udate.getTime());
		return sqlDate;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 17, 2018
	 * @Description :to get previous date of given date
	 * @Return : String
	 */
	public static String getYesterdayDateString(final String date, final String date_format) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(date_format);
		Date util_date = dateFormat.parse(date);
		final Calendar cal = Calendar.getInstance();
		cal.setTime(util_date);
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

	public static String timestampToString(Timestamp mTimestamp) {
		java.sql.Date date = new java.sql.Date(mTimestamp.getTime());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(date);
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 17, 2018
	 * @Description :to calculate day difference between given date.
	 * @Return : long
	 */
	public static long getDayDiffFromStringDate(final String previous_date, final String next_date,
			final String date_format) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(date_format);
		Date prev_date = format.parse(previous_date);
		Date nex_date = format.parse(next_date);// greater date
		// in milliseconds
		long diff = nex_date.getTime() - prev_date.getTime();
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffDays;
	}

	/**
	 * 
	 * @author Ashok@
	 * @throws ParseException
	 * @Date : Sep 17, 2018
	 * @Description : to covert String date into MySql date.
	 * @Return : Timestamp (in given formate)
	 */
	public static Timestamp stringToTimestamp(final String date, final String date_format) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(date_format);
		Date parsed_date = format.parse(date);
		Timestamp timestamp = new Timestamp(parsed_date.getTime());
		return timestamp;
	}

	public static String formatDate(String _date, String _fromFormat, String _toFormat) throws ParseException {
		String strDate = "";
		SimpleDateFormat format1 = new SimpleDateFormat(_fromFormat);
		SimpleDateFormat format2 = new SimpleDateFormat(_toFormat);
		Date date = format1.parse(_date);
		strDate = format2.format(date);
		return strDate;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 17, 2018
	 * @Description :to get sql current timestamp.
	 * @Return : java.sql.Date
	 */
	public static java.sql.Timestamp getCurrentSQLTimeStamp() {
		java.util.Date date = new java.util.Date();
		java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
		return timestamp;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 12, 2018
	 * @Description :to decrypt string and convert String into JSONObject.
	 * @Return : JSONObject
	 */
	public static String decryptString(final String data, final String key) throws JSONException {
		if (data != null) {
			return AESEncryption.decrypt(data, key);
		} else {
			throw new NullPointerException();
		}
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 12, 2018
	 * @Description :to convert input string data into JSONObject.
	 * @Return : JSONObject
	 */
	public static JSONObject string_to_JsonObject(final String data) throws JSONException {
		if (data != null) {
			return new JSONObject(data);
		} else {
			throw new NullPointerException();
		}
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 12, 2018
	 * @Description :to set response message.
	 * @Return : ResponseModel
	 */
	public static ResponseModel setResponse(final Integer status_code, final String response_message,
			final Object myObjectList) {
		ResponseModel response = new ResponseModel();
		response.setStatus(status_code);
		response.setMessage(response_message);
		response.setMyObjectList(myObjectList);
		return response;
	}

	/**
	 * 
	 * @author Nikhil Sharma
	 * @Date : 14-Jan-2020
	 * @Description : To set API response v2
	 * @Return : ResponseModel
	 */
	public static ResponseModelv2 setResponseV2(final Integer status_code, final String response_message,
			final Object myObjectList) {
		ResponseModelv2 response = new ResponseModelv2();
		response.setStatus(status_code);
		response.setMessage(response_message);
		response.setMyObjectList(myObjectList);
		return response;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Aug 4, 2018
	 * @Description :to write multipart file in specified path.
	 * @Return : void
	 */
	public static boolean fileWriterMultipart(final String path_name, MultipartFile file) throws IOException {
		boolean response = true;
		FileOutputStream outputStream = null;
		BufferedInputStream inputStream = null;
		try {
			byte[] bytes = file.getBytes();
			inputStream = new BufferedInputStream(new ByteArrayInputStream(bytes));
			File rootPath = new File(path_name);
			outputStream = new FileOutputStream(rootPath);
			byte[] buffer = new byte[2048];
			int length;
			while ((length = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, length);

			}
			System.out.println("File written Successfully");
			response = true;
		} catch (Exception e) {
			Logger.getPathInstance().log(e.getMessage(), Util.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		} finally {
			if (inputStream != null && outputStream != null) {
				inputStream.close();
				outputStream.close();
			}
		}
		return response;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Aug 6, 2018
	 * @Description :used to write buyte array into file.
	 * @Return : boolean
	 */
	public static boolean fileWriterByteArray(final String path_name, byte[] bytes) throws IOException {
		boolean response = true;
		FileOutputStream outputStream = null;
		BufferedInputStream inputStream = null;
		try {
			inputStream = new BufferedInputStream(new ByteArrayInputStream(bytes));
			File rootPath = new File(path_name);
			outputStream = new FileOutputStream(rootPath);
			byte[] buffer = new byte[2048];
			int length;
			while ((length = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, length);
			}
			System.out.println("File written Successfully");
			response = true;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (inputStream != null && outputStream != null) {
				inputStream.close();
				outputStream.close();
			}
		}
		return response;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 15, 2018
	 * @Description :to generate basic authentication.
	 * @Return : String
	 */
	public static String generateBasicAuthTokken(final String username, final String password) {
		String authString = username + ":" + password;
		byte[] authEncBytes = Base64.getEncoder().encode(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		return authStringEnc;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 15, 2018
	 * @Description :to decode authentication header and retrieve username and
	 *              password.
	 * @Return : String
	 */
	public static String getBasicAuthTokken(final String basicAuth) {
		/*
		 * byte[] authEncBytes = Base64.getDecoder().decode(basicAuth.getBytes());
		 * String authStringEnc = new String(authEncBytes);
		 */

		byte[] authEncBytes = Base64.getDecoder().decode(basicAuth.getBytes());
		String authStringEnc = new String(authEncBytes);
		return authStringEnc;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 15, 2018
	 * @Description :to convert JSONString into JSONObject.
	 * @Return : JSONObject
	 */
	public static JSONObject stringToJSONObject(final String data) throws JSONException {
		JSONObject mJsonObject = new JSONObject(data);
		return mJsonObject;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 15, 2018
	 * @Description :to generate alphanumeric random string (used to generate
	 *              session tokken)
	 * @Return : String
	 */
	public static String generateRandomAlphaNumeric(int size) {
		StringBuilder builder = new StringBuilder();
		while (size-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 18, 2018
	 * @Description :to get root location
	 * @Return : Path
	 */
	public static Path initRoot() {
		Path rootLocation = null;
		String s = System.getProperty("os.name");
		if (s.contains(AppConstant.WINDOW)) {
			rootLocation = Paths.get("./" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.WEB_ROOT_DIRECTORY);
		} else if (s.equalsIgnoreCase(AppConstant.LINUX)) {
			rootLocation = Paths.get("/home/" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.WEB_ROOT_DIRECTORY);
		}
		return rootLocation;
	}

	public static Path initPropertyRoot() {
		Path rootLocation = null;
		String s = System.getProperty("os.name");
		if (s.contains(AppConstant.WINDOW)) {
			rootLocation = Paths.get("./" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.CONFIG_DIRECTORY);
		} else if (s.equalsIgnoreCase(AppConstant.LINUX)) {
			rootLocation = Paths.get("/home/" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.CONFIG_DIRECTORY);
		}
		return rootLocation;
	}


	/**
	 * 
	 * @author Ashok@
	 * @Date : Oct 8, 2018
	 * @Description : to initialize mobile root directory.
	 * @Return : Path
	 */
	public static Path initMobileRoot() {
		Path rootLocation = null;
		String s = System.getProperty("os.name");
		if (s.contains(AppConstant.WINDOW)) {
			rootLocation = Paths.get("./" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.MOBLIE_ROOT_DIRECTORY);
		} else if (s.equalsIgnoreCase(AppConstant.LINUX)) {
			rootLocation = Paths.get("/home/" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.MOBLIE_ROOT_DIRECTORY);
		}
		return rootLocation;
	}

	public static Path initMobileChallanRoot() {
		Path rootLocation = null;
		String s = System.getProperty("os.name");
		if (s.contains(AppConstant.WINDOW)) {
			rootLocation = Paths.get("./" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.MOBLIE_ROOT_DIRECTORY);
		} else if (s.equalsIgnoreCase(AppConstant.LINUX)) {
			rootLocation = Paths.get("/home/" + AppConstant.ROOT_DIRECTORY + "/" + AppConstant.MOBLIE_ROOT_DIRECTORY);
		}
		return rootLocation;
	}

	public static String getTransitStartTime() {
		Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
		calendar.add(Calendar.MINUTE, -3);
		SimpleDateFormat sdf = new SimpleDateFormat("'D'yyyyMMdd'_H'HHmmss'_M'SSS");
		System.out.println("\n Start time === " + sdf.format(calendar.getTime()) + "\n ");
		return sdf.format(calendar.getTime());
	}

	public static String getTransitEndTime() {
		Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
		// calendar.add(Calendar.MINUTE, -2);
		SimpleDateFormat sdf = new SimpleDateFormat("'D'yyyyMMdd'_H'HHmmss'_M'SSS");
		System.out.println("\n End time === " + sdf.format(calendar.getTime()) + "\n  ");
		return sdf.format(calendar.getTime());
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 19, 2018
	 * @Description :to remove characters from transit date.
	 * @Return : String
	 */
	public static String formatTransitDate(final String date) {
		String s = date.replace("D", "");
		String s1 = s.replace("_H", "");
		String formatedDate1 = s1.replace("_M", "");
		// String formatedDate = formatedDate1.substring(0, formatedDate1.length() - 3);
		return formatedDate1;
	}

	

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 26, 2018
	 * @Description :to get 90 days before current date.
	 * @Return : Date
	 */
	public static String getDeleteDate(final int month, final int dayOfMonth, final int hour) throws Exception {
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.MONTH, month);
		cal.add(Calendar.DAY_OF_MONTH, dayOfMonth);
		cal.add(Calendar.HOUR, hour);
		Date before_date = cal.getTime();
		SimpleDateFormat sformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sformat.format(before_date);
		System.out.println("record date = " + date);
		return date;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Sep 29, 2018
	 * @Description : to create directory (re)
	 * @Return : Path
	 */
	public static Path createDirectory(final Path file_path) {
		Path imagePath = null;
		try {
			if (!Files.exists(file_path)) {
				imagePath = Files.createDirectories(file_path);
				System.out.println("Directory created");
			} else {
				System.out.println("Directory already exist");
				imagePath = file_path;
			}
		} catch (Exception e) {
			Logger.getPathInstance().log(e.getMessage(), Util.class.getSimpleName(), AppConstant.ERROR_STATUS_CODE,
					e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		}
		return imagePath;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Oct 8, 2018
	 * @Description :to generate random challan ID.
	 * @Return : String
	 */
	public static String generateUniqueIncidentID(final String _prifix, final int count) {
		// final String stateName = "DEH";
		final String challanID = _prifix + getCurrentDateTimeStamp() + "-" + count;// + getSaltString()
		return challanID;
	}

	protected static String getSaltString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 6) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

	public static String geyIncidentID(final String _prifix) {
		// final String stateName = "DEH";
		final String challanID = _prifix + getCurrentDateTimeStamp();// + getSaltString()
		return challanID;
	}

	public static String generateReceiptID(final String _prifix) {
		// final String stateName = "DEH";
		final String challanID = _prifix + getCurrentDateTimeStamp() + getSaltString();
		return challanID;
	}

	public static String getCurrentDateTimeStamp() {
		/*
		 * Calendar calendar = Calendar.getInstance(Locale.getDefault()); return
		 * calendar.getTimeInMillis();
		 */
		Timestamp ts = Util.getCurrentSQLTimeStamp();
		Date date = new Date();
		date.setTime(ts.getTime());
		String formattedDate = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
		return formattedDate;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Oct 12, 2018
	 * @Description :input stream into Bytes
	 * @Return : byte[]
	 */
	public static byte[] getBytes(InputStream is) throws IOException {

		int len;
		int size = 1024;
		byte[] buf;

		if (is instanceof ByteArrayInputStream) {
			size = is.available();
			buf = new byte[size];
			len = is.read(buf, 0, size);
		} else {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			buf = new byte[size];
			while ((len = is.read(buf, 0, size)) != -1)
				bos.write(buf, 0, len);
			buf = bos.toByteArray();
		}
		return buf;
	}

	public static JSONObject convertPOJOToJSON(final Object mModelObject) {
		ObjectMapper objectMapper = new ObjectMapper();
		JSONObject mJsonObject = null;
		String jsonObject;
		try {
			jsonObject = objectMapper.writeValueAsString(mModelObject);
			mJsonObject = new JSONObject(jsonObject);
		} catch (JSONException e) {
			Logger.getPathInstance().log(e.getMessage(), Util.class.getSimpleName(), AppConstant.ERROR,
					e.getStackTrace()[0].getLineNumber());
			e.printStackTrace();
		} catch (JsonProcessingException e1) {
			Logger.getPathInstance().log(e1.getMessage(), Util.class.getSimpleName(), AppConstant.ERROR,
					e1.getStackTrace()[0].getLineNumber());
			e1.printStackTrace();
		}
		return mJsonObject;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Oct 17, 2018
	 * @Description :to convert sql date into sql timestamp
	 * @Return : Timestamp
	 */
	public static Timestamp sqlDateToSqlTimestamp(java.sql.Date date) throws Exception {
		java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
		return timestamp;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Oct 17, 2018
	 * @Description :to convert timestamp into date.
	 * @Return : Date
	 */
	public static String timestampToDate(Timestamp mTimestamp) {
		java.sql.Date date = new java.sql.Date(mTimestamp.getTime());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Feb 3, 2018
	 * @Description :method to get minute and degree
	 * @Return : double
	 */
	public static double getCordinates(final String cordinate) {
		if (cordinate != null) {
			if (!cordinate.isEmpty()) {
				int index = cordinate.indexOf(".");
				String minute = cordinate.substring(index - 2, cordinate.length() - 2);
				System.out.println("Minute = " + minute);
				String degree = null;
				if (index % 2 == 0) {
					degree = cordinate.substring(0, index - 2);
					System.out.println("degree while even = " + degree);
					return calculateCordinates(Integer.valueOf(degree), Double.valueOf(minute));
				} else {
					degree = cordinate.substring(0, index - 2);
					System.out.println("degree while odd = " + degree);
					return calculateCordinates(Integer.valueOf(degree), Double.valueOf(minute));
				}
			}
		} else {
			return 0.0;
		}
		return 0.0;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Feb 3, 2018
	 * @Description :Method to calculate coordinates using degree and minutes
	 * @Return : double
	 */
	private static double calculateCordinates(int degree, double minute) {
		double cordinate = degree + (minute / 60);
		System.out.println("Co-ordinate = " + cordinate);
		return cordinate;
	}

	
	/**
	 * 
	 * @author Ashok@
	 * @throws ParseException
	 * @Date : Jan 19, 2019
	 * @Description :to format timestamp to transit request timestamp.
	 * @Return : String
	 */
	public static String formatTransitReqDataTime(String timestamp, boolean isDataBase) throws ParseException {
		Timestamp ts = null;
		if (isDataBase) {
			ts = stringToTimestamp(timestamp, "yyyy-MM-dd HH:mm:ss");
		} else {
			ts = stringToTimestamp(timestamp, "yyyy-MM-dd HH:mm:ss.SSS");
		}
		ts = addOneMillisecond(ts);
		Date date = new Date();
		date.setTime(ts.getTime());
		String formattedDate = new SimpleDateFormat("'D'yyyyMMdd'_H'HHmmss'_M'SSS").format(date);
		return formattedDate;
	}

	public static Timestamp addOneMillisecond(Timestamp timestamp) {
		System.out.println("Timestamp before add 1milli = " + timestamp);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timestamp.getTime());
		// cal.add(Calendar.MILLISECOND, 1);
		cal.add(Calendar.MILLISECOND, 200);
		Timestamp newTimestamp = new Timestamp(cal.getTime().getTime());
		System.out.println("Timestamp after add 1milli = " + newTimestamp);
		return newTimestamp;
	}

	/**
	 * 
	 * @author Ashok@
	 * @Date : Feb 4, 2019
	 * @Description :to add or subtract month from given date.
	 * @Return : Date
	 */
	public static String addOrSubtractMonth(final Timestamp challanTimestamp, final int month) throws Exception {
		Date date = new Date(challanTimestamp.getTime());
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.MONTH, month);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String formatedDate = formatter.format(cal.getTime());
		System.out.println("previous date to check = " + formatedDate);
		return formatedDate;
	}

	

	/**
	 * 
	 * @author Ashok@
	 * @Date : Jan 31, 2019
	 * @Description :to check if file is excel format or not.
	 * @Return : boolean
	 */
	public static boolean checkFileType(MultipartFile file) {
		boolean response = false;
		String filename = file.getOriginalFilename().toLowerCase();
		if (filename.endsWith(".xlsx")) {
			response = true;
		} else if (filename.endsWith(".xls")) {
			response = true;
		} else if (filename.endsWith(".xlsb")) {
			response = true;
		} else if (filename.endsWith(".xlsm")) {
			response = true;
		} else {
			response = false;
		}

		return response;
	}

	

	public static String addOrSubtractDay(final String date, final String date_format, int day) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(date_format);
		Date util_date = dateFormat.parse(date);
		final Calendar cal = Calendar.getInstance();
		cal.setTime(util_date);
		cal.add(Calendar.DATE, day);
		return dateFormat.format(cal.getTime());
	}

	/**
	 * 
	 * @author : @shok@ 23-Oct-2019
	 * @return String getCurrentYear
	 * @Description : to get current year
	 */
	public static String getCurrentYear() {
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		return String.valueOf(year);
	}

	/**
	 * 
	 * @author : @shok@ 23-Oct-2019
	 * @return String getCurrentMonth
	 * @Description : to get current month.
	 */
	public static String getCurrentMonth() {
		return new SimpleDateFormat("MMMM").format(new Date()).toUpperCase();
	}

	/**
	 * 
	 * @author : @shok@ 23-Oct-2019
	 * @return String appendYearMonthPath
	 * @Description : to get year/monthName path
	 */
	public static String appendYearMonthPath() {
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String month = new SimpleDateFormat("MMMM").format(new Date());
		return year + "/" + month.toUpperCase();
	}

	/**
	 * 
	 * @author : @shok@ 05-Nov-2019
	 * @param timestamp
	 * @return String getYearMonthFromSqlTimestamp
	 * @Description : to get year and monthName from given SQL timestamp
	 *              (2019/NOVEMBER).
	 */
	public static String getYearMonthFromSqlTimestamp(final Timestamp timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(timestamp);
		int year = cal.get(java.util.Calendar.YEAR);
		String monthName = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
		return year + "/" + monthName.toUpperCase().trim();
	}

	/**
	 * 
	 * @author : @shok@ 05-Nov-2019
	 * @param timestamp
	 * @return int getYearFromSqlTimestamp
	 * @Description : to get year from given timestamp.
	 */
	public static int getYearFromSqlTimestamp(final Timestamp timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(timestamp);
		int year = cal.get(java.util.Calendar.YEAR);
		return year;
	}

	/**
	 * 
	 * @author : @shok@ 05-Nov-2019
	 * @param timestamp
	 * @return String, getMonthNameFromSqlTimestamp
	 * @Description : to get monthName from given timestamp.
	 */
	public static String getMonthNameFromSqlTimestamp(final Timestamp timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(timestamp);
		String monthName = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
		return monthName.toUpperCase().trim();
	}
}
